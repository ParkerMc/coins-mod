package net.parkermc.coins;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.io.WritingMode;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;

import java.nio.file.Path;

@Mod.EventBusSubscriber(modid = CoinsMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class CoinsConfig {

    public static final String CATEGORY_GENERAL = "general";
    public static ForgeConfigSpec.ConfigValue<String> HOVER_TEXT;

    private static final ForgeConfigSpec.Builder BUILDER = new ForgeConfigSpec.Builder();
    public static ForgeConfigSpec SPEC;


    static {

        BUILDER.comment("General settings").push(CATEGORY_GENERAL);
        HOVER_TEXT = BUILDER.comment("The text that appears when you hover over the coin.").define("hoverText", "");
        BUILDER.pop();

        SPEC = BUILDER.build();
    }


    public static void loadConfig(ForgeConfigSpec spec, Path path) {

        final CommentedFileConfig configData = CommentedFileConfig.builder(path)
                .sync().autosave().writingMode(WritingMode.REPLACE).build();

        configData.load();
        spec.setConfig(configData);
    }
}
