package net.parkermc.coins.items;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.parkermc.coins.CoinsConfig;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;


public class CoinItem extends Item {
	public static final String unlocalizedNameBase = "coin";
	
	public CoinItem(String amt) {
		super(new Properties());
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void appendHoverText(@NotNull ItemStack stack, @Nullable Level level, @NotNull List<Component> textComponents, @NotNull TooltipFlag tooltipFlag) {
		if(CoinsConfig.HOVER_TEXT.get().isEmpty()){
			return;
		}

		textComponents.add(Component.literal(CoinsConfig.HOVER_TEXT.get()).withStyle(ChatFormatting.GRAY));
	}
}
