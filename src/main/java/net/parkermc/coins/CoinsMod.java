package net.parkermc.coins;

import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;
import net.parkermc.coins.items.CoinsItems;

@Mod(CoinsMod.MODID)
public class CoinsMod {
    public static final String MODID = "parkermc_coins";
    private static CreativeModeTab creativeTab;
    private static final DeferredRegister<CreativeModeTab> CREATIVE_TAB_REGISTER = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, CoinsMod.MODID);

    public CoinsMod() {
        var modEventBus = FMLJavaModLoadingContext.get().getModEventBus();
        CoinsItems.ITEMS.register(modEventBus);
        CREATIVE_TAB_REGISTER.register(modEventBus);

        ModLoadingContext.get().registerConfig(net.minecraftforge.fml.config.ModConfig.Type.COMMON, CoinsConfig.SPEC);
    }


    public static final RegistryObject<CreativeModeTab> TEST_TAB = CREATIVE_TAB_REGISTER.register("coins",
            () -> CreativeModeTab.builder()
                    .title(Component.translatable("itemGroup." + MODID))
                    .icon(() -> new ItemStack(CoinsItems.coin_10.get()))
                    .displayItems((pParameters, pOutput) -> {
                        pOutput.accept(CoinsItems.coin_10.get());
                        pOutput.accept(CoinsItems.coin_100.get());
                        pOutput.accept(CoinsItems.coin_1k.get());
                        pOutput.accept(CoinsItems.coin_10k.get());
                        pOutput.accept(CoinsItems.coin_1m.get());
                    })
                    .build());
}

